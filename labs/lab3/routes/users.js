let express = require("express");
let router = express.Router();

let storage = require("./../modules/storage");

router.get("/", (req, res, next) => {
    res.render("users");
});

router.get("/:user_id(\\d+)",
    (req, res, next) => {
        res.render("user");
    });

module.exports = router;