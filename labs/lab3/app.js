const express = require("express");
const bodyParser = require("body-parser");
const busboyBodyParser = require("busboy-body-parser");
const storage = require("./modules/storage");

const users = require("./routes/users");

const app = express();

let sexes = storage.body;

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(busboyBodyParser());

app.use("/users", users);

app.get("/", (req, res) => res.render("index",{}));

app.listen(3000, () => console.log("UP!"));