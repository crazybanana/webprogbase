const storage = require("./storage");

let askString = "Input command: ";

process.stdin.setEncoding("utf8");

process.stdin.on("readable", () => {
    const chunk = process.stdin.read();
    if (chunk !== null) {
        let inputString = chunk.toString().trim();
        if (inputString === "create()") {
            storage.create().then(returnedString => {
                storage.body.push(returnedString);
                console.log(storage.body);
                return askString;
            }).then(commandString => {
                process.stdout.write(commandString);
            }).then(() => {
                storage.saveToFile();
            });
        }
        if (inputString === "getAll()") {
            storage.getAll().then(returnedString => {
                console.log(returnedString);
                return askString;
            }).then(commandString => {
                process.stdout.write(commandString);
            });
        }
        if (inputString.search(regExpGetById) !== -1) {
            const regExpNumber = /\d+/g;
            const number = inputString.toString().trim().match(regExpNumber);
            storage.getById(number).then(returnedString => {
                console.log(returnedString);
                return askString;
            }).then(commandString => {
                process.stdout.write(commandString);
            }).then(() => {
                storage.saveToFile();
            });
        }
        if (inputString.search(regExpUpdate) !== -1) {
            const regExpNumber = /\d+/g;
            const number = inputString.toString().trim().match(regExpNumber);
            storage.update(number).then(returnedString => {
                storage.remove(number).then((returnedInt) => {
                    storage.body.splice(returnedInt, 1, returnedString);
                });
                return askString;
            }).then(commandString => {
                process.stdout.write(commandString);
            }).then(() => {
                storage.saveToFile();
            });
        }
        if (inputString.search(regExpRemove) !== -1) {
            const regExpNumber = /\d+/g;
            const number = inputString.toString().trim().match(regExpNumber);
            storage.remove(number).then(returnedInt => {
                storage.body.splice(returnedInt, 1);
                return askString;
            }).then(commandString => {
                process.stdout.write(commandString);
            }).then(() => {
                storage.saveToFile();
            });
        }
    }
});

process.stdin.on("end", () => {
    process.stdout.write("end");
});

function askQuestion() {
    process.stdout.write("Input command: ");
}

const regExpGetById = /[g][e][t][B][y][I][d][(]\d+[)]/g;
const regExpRemove = /[r][e][m][o][v][e][(]\d+[)]/g;
const regExpUpdate = /[u][p][d][a][t][e][(]\d+[)]/g;

askQuestion();