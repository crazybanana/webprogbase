const fs = require("fs-promise");
const prompt = require("prompt");

prompt.start();

let body = [];

fileCheck();

function saveToFile() {
    fs.writeFile("../users.json",
        JSON.stringify(body, null, 4),
        (err) => {
            if (err) throw err;
        });
}

function fileCheck() {
    const promise = fs.readFile("../users.json");
    promise.then(fileText => {
        body = JSON.parse(fileText);
    }).catch(() => {
        return;
    });
}

function claimId() {
    let random = Math.floor(Math.random() * (100000 - 0)) + 0;
    if (body.lenght === 0) return random;
    else {
        body.forEach((value) => {
            if (value.id === random) {
                return claimId();
            } else return random;
        });
    }
    return random;
}

function create() {
    return new Promise((resolve, reject) => {
        prompt.get(user,
            (err, result) => {
                const userBuffer = {
                    id: claimId(),
                    name: result.name,
                    surname: result.surname,
                    points: result.points,
                    date: result.date
                }
                resolve(userBuffer);
            });
    });
}

function getAll() {
    return new Promise((resolve, reject) => {
        if (body.length !== 0) resolve(body);
        else reject("Empty");
    });
}

function getById(userId) {
    let answer;
    body.forEach((value) => {
        if (value.id == userId) {
            answer = value;
        }
    });
    return new Promise((resolve, reject) => {
        if (userId < 0) reject("Id error");
        if (body.lenght <= 0) {
            reject("Empty");
        }
        resolve(answer);
    });
}

function update(userId) {
    return new Promise((resolve, reject) => {
        if (userId < 0) reject("Id error");
        if (body.lenght <= 0) {
            reject("Empty");
        }
        body.forEach((value) => {
            if (value.id == userId) {
                prompt.get(["field", "toChange"],
                    (err, result) => {
                        switch (result.field) {
                            case "name":
                                value.name = result.toChange;
                                resolve(value);
                                break;
                            case "surname":
                                value.surname = result.toChange;
                                resolve(value);
                                break;
                            case "points":
                                value.points = result.toChange;
                                resolve(value);
                                break;
                            case "date":
                                value.date = result.toChange;
                                resolve(value);
                                break;
                            default:
                                reject("ERROR");
                                break;
                        }
                    });
            }
        });
    });
}

function remove(userId) {
    let answer;
    body.forEach((value) => {
        if (value.id == userId) {
            answer = body.indexOf(value);
        }
    });
    return new Promise((resolve, reject) => {
        if (userId < 0) reject("Id error");
        if (body.lenght <= 0) {
            reject("Empty");
        }
        resolve(answer);
    });
}

let user = {
    properties: {
        name: {
            description: "Enter your name",
            type: "string",
            pattern: /^[a-zA-Z]+$/,
            message: "Name must be only letters",
            required: true
        },
        surname: {
            description: "Enter your surname",
            type: "string",
            pattern: /^[a-zA-Z]+$/,
            message: "Surname must be only letters",
            required: true
        },
        points: {
            description: "Enter your score",
            pattern: /^[1-5][.][0-9]$/,
            message: "Score must be like N.N and less then 5.0",
            required: true
        },
        date: {
            description: "Enter your date",
            pattern: /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/,
            message: "Score must be only YYYY-MM-DD",
            required: true
        }
    }
};

module.exports = {
    create,
    getAll,
    getById,
    update,
    remove,
    saveToFile,
    body,
    fileCheck
}