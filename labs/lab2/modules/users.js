const fs = require("fs-promise");
const prompt = require("prompt");
const readline = require("readline");

prompt.start();

let body = [];
let askString = "Input command: ";

process.stdin.setEncoding("utf8");

process.stdin.on("readable", () => {
    fileCheck();
    const chunk = process.stdin.read();
    if (chunk !== null) {
        Promise.resolve(chunk).then((inputString) => {
            if (inputString.toString().trim() === "create()") {
                create().then(returnedString => {
                    body.push(returnedString);
                    console.log(body);
                    return askString;
                }).then(commandString => {
                    process.stdout.write(commandString);
                }).then(() => {
                    saveToFile();
                });
            }
            if (inputString.toString().trim() === "getAll()") {
                getAll().then(returnedString => {
                    console.log(JSON.parse(returnedString));
                    return askString;
                }).then(commandString => {
                    process.stdout.write(commandString);
                });
            }
            if (inputString.toString().trim().search(regExpGetById) !== -1) {
                const regExpNumber = /\d+/g;
                const number = inputString.toString().trim().match(regExpNumber);
                getById(number).then(returnedString => {
                    console.log(returnedString);
                    return askString;
                }).then(commandString => {
                    process.stdout.write(commandString);
                }).then(() => {
                    saveToFile();
                });
            }
            if (inputString.toString().trim().search(regExpUpdate) !== -1) {
                const regExpNumber = /\d+/g;
                const number = inputString.toString().trim().match(regExpNumber);
                update(number).then(returnedString => {
                    console.log(returnedString);
                    return askString;
                }).then(commandString => {
                    process.stdout.write(commandString);
                }).then(() => {
                    saveToFile();
                });
            }
            if (inputString.toString().trim().search(regExpRemove) !== -1) {
                const regExpNumber = /\d+/g;
                const number = inputString.toString().trim().match(regExpNumber);
                remove(number).then(returnedInt => {
                    body.splice(returnedInt, 1);
                    return askString;
                }).then(commandString => {
                    process.stdout.write(commandString);
                }).then(() => {
                    saveToFile();
                });
            }
        });
    }
});

process.stdin.on("end", () => {
    process.stdout.write("end");
});

function fileCheck() {
    const promise = fs.readFile("users.json");
    promise.then(fileText => {
        body = JSON.parse(fileText);
    }).catch(() => {
        return;
    });
}

function saveToFile() {
    fs.writeFile("users.json",
        JSON.stringify(body, null, 4),
        (err) => {
            if (err) throw err;
        });
}

function askQuestion() {
    process.stdout.write("Input command: ");
}

function checkId() {
    let random = Math.floor(Math.random() * (100000 - 0)) + 0;
    if (body.lenght === 0) return random;
    else {
        body.forEach((value) => {
            if (value.id === random) {
                return random + random;
            } else return random;
        });
    }
    return random;
}

function create() {
    return new Promise((resolve, reject) => {
        prompt.get(user,
            (err, result) => {
                const userBuffer = {
                    id: checkId(),
                    name: result.name,
                    surname: result.surname,
                    points: result.points,
                    date: result.date
                }
                resolve(userBuffer);
            });
    });
}

function getAll() {
    return new Promise((resolve, reject) => {
        if (body.length !== 0) resolve(body);
        else reject("Empty");
    });
}

function getById(userId) {
    let answer;
    body.forEach((value) => {
        if (value.id == userId) {
            answer = value;
        }
    });
    return new Promise((resolve, reject) => {
        if (userId < 0) reject("Id error");
        if (body.lenght <= 0) {
            reject("Empty");
        }
        resolve(answer);
    });
}

function update(userId) {
    return new Promise((resolve, reject) => {
        if (userId < 0) reject("Id error");
        if (body.lenght <= 0) {
            reject("Empty");
        }
        body.forEach((value) => {
            if (value.id == userId) {
                prompt.get(["field", "toChange"],
                    (err, result) => {
                        switch (result.field) {
                        case "name":
                            value.name = result.toChange;
                            resolve(value);
                            break;
                        case "surname":
                            value.surname = result.toChange;
                            resolve(value);
                            break;
                        case "points":
                            value.points = result.toChange;
                            resolve(value);
                            break;
                        case "date":
                            value.date = result.toChange;
                            resolve(value);
                            break;
                        default:
                            break;
                        }
                    });
            }
        });
    });
}

function remove(userId) {
    let answer;
    body.forEach((value) => {
        if (value.id === userId) {
            answer = body.indexOf(value);
        }
    });
    return new Promise((resolve, reject) => {
        if (userId < 0) reject("Id error");
        if (body.lenght <= 0) {
            reject("Empty");
        }
        resolve(answer);
    });
}

let user = {
    properties: {
        name: {
            description: "Enter your name", 
            type: "string",   
            pattern: /^[a-zA-Z]+$/,
            message: "Name must be only letters",
            required: true
        },
        surname: {
            description: "Enter your surname",
            type: "string",   
            pattern: /^[a-zA-Z]+$/,
            message: "Surname must be only letters",
            required: true
        },
        points: {
            description: "Enter your score", 
            pattern: /^[1-5][.][0-9]$/,
            message: "Score must be like N.N and less then 5.0",
            required: true
        },
        date: {
            description: "Enter your date", 
            pattern: /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/,
            message: "Score must be only YYYY-MM-DD",
            required: true
        }
    }
};

const regExpGetById = /[g][e][t][B][y][I][d][(]\d+[)]/g;
const regExpRemove = /[r][e][m][o][v][e][(]\d+[)]/g;
const regExpUpdate = /[u][p][d][a][t][e][(]\d+[)]/g;

askQuestion();