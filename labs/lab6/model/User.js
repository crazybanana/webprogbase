const mongoose = require("mongoose");
const validate = require("mongoose-validator");
const shortid = require('shortid');

const Schema = mongoose.Schema;

let userSchema = new Schema({
    username: {
        type: String,
        required: true,
        minlength: 4,
        maxlength: 20,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String
    }
});

let User = mongoose.model("User", userSchema);

module.exports = User;