const mongoose = require("mongoose");
const validate = require("mongoose-validator");

const Schema = mongoose.Schema;

let nameValidator = [
    validate({
        validator: "isLength",
        arguments: [3, 50],
        message: "Имя должно быть между {ARGS[0]} и {ARGS[1]} символами"
    })
];

let storageSchema = new Schema({
    id: {
        type: String
    },
    name: {
        type: String,
        required: true,
        validate: nameValidator
    },
    city: {
      type: String,
      required: true
    },
    condition: {
        type: String,
        required: true
    },
    about: {
        type: String
    },
    image: {
        type: String
    },
    imageID: {
        type: String
    },
    ownerID:{
        type: String
    }
});

let Storage = mongoose.model("Storage", storageSchema);

module.exports = Storage;