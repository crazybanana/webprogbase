const express = require("express");
const bodyParser = require("body-parser");
const busboyBodyParser = require("busboy-body-parser");
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require("mongoose");

//js
const Storage = require("./model/Storage");
const config = require("./js/config");

//routes
const goods = require("./routes/goods");
const profile = require('./routes/profile');

const app = express();

//database
mongoose.connect("mongodb://goods:Crazybanana123@ds149535.mlab.com:49535/heroku_8zs60hrv", {
    useMongoClient: true
});
// mongoose.connect("mongodb://localhost/myappdatabase", {
//     useMongoClient: true
// });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('Connected to mongodb successfully.')
});

//server setups
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(busboyBodyParser({ limit: '5mb' }));
app.use(session({
    secret: "webtj947tb$*$)974e",
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use("/goods", goods);
app.use("/profile", profile);

//server requests
app.get("/",
     (req, res) =>{
        res.render('index', {
            user: req.user
        })
    });

app.get("/search",
    bodyParser.urlencoded({extended: false}),
     async (req, res) => {
        let storage = await Storage.find();
        let filter = [];
         storage.forEach(function (object) {
             if((object.name).indexOf(req.query.search) !== -1){
                 filter.push(object);
             }
         });


        let itemsCount = filter.length;
        let pageSize = 5;
        let pagesCount = Math.ceil(itemsCount / 5);
        let currentPage = 1;
        let itemsArrays = [];
        let itemsList = [];

        while(filter.length > 0){
            itemsArrays.push(filter.splice(0, pageSize));
        }

        if (typeof req.query.page !== 'undefined') {
             currentPage = +req.query.page;
        }

        itemsList = itemsArrays[+currentPage - 1];

        if(itemsCount !== 0){
            res.render("filter", {
                founded: true,
                user: req.user,
                searchParam: req.query.search,
                object: itemsList,
                pageSize: pageSize,
                pagesCount: pagesCount,
                totalItems: itemsCount,
                currentPage: currentPage
            });
        }else{
            res.render("filter", {object: "Ничего не найдено",
                founded: false,
                user: req.user});
        }
    });

app.use((req, res) => {
    res.sendStatus(404);
});

app.listen(config.port, () => console.log("UP!"));