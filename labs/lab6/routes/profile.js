const express = require("express");
const crypto = require('crypto');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const shortid = require('shortid');
const router = express.Router();

const User = require('./../model/User');

router.use(express.static("public"));

//passport
passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (_id, done) {
    let user = User.find();
    User.findOne({'_id': _id})
        .then(user => {
            if (!user) done("No user des");
            else done(null, user);
        });
});

passport.use(new LocalStrategy(
    function (username, password, done) {
        let passwordHash = sha512(password, serverSalt).passwordHash;
        User.findOne({username: username, password: passwordHash})
            .then(user =>{
                if(!user) return done("No user local", false);
                return done(null, user);
            });
    }
));

router.use(cookieParser());
router.use(session({
    secret: "webtj947tb$*$)974e",
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());

router.get("/",
    checkAuth,
    async (req, res) =>{
        let user = req.user;
        res.render('profile', {
            user
        })
    });

router.get('/admin',
    checkAdmin,
    async (req, res) => {
        let allUsers = await User.find();
        res.render('admin', {
            user: req.user,
            allUsers: allUsers
        });
    });

router.get('/logout',
    (req, res) => {
        req.logout();
        res.redirect('login');
    });

router.get('/register',
    async (req, res) => {
        if(req.user){
            res.redirect("/profile");
        }else{
            res.render("register", {user: req.user});
        }
    });

router.get("/login",
    async (req, res) => {
        if(req.user){
            res.redirect("/profile");
        }else if(!req.user){
            res.render("login", {user: req.user});
        }else{
            res.redirect("/profile");
        }
    });

router.post('/login',
    passport.authenticate('local'),
    (req, res) => res.redirect('/profile'));

router.post("/register",
    async (req, res) => {
        let username = req.body.username;
        let password = req.body.password;
        let passwordHash = sha512(password, serverSalt).passwordHash;

        try{
            let user = new User({
                username,
                password: passwordHash,
                role: "user"
            });
            await user.save(function (err) {
                if(err) throw err;
            });
            res.redirect("login");
        }catch (err){
            res.sendStatus(500);
        }
    });

//functions
function checkAdmin(req, res, next) {
    if (!req.user) return res.redirect("/login");
    if (req.user.role !== 'admin') return res.sendStatus(403);
    next();
}

function checkAuth(req, res, next) {
    if (!req.user) return res.redirect("/login");
    next();
}

const serverSalt = "ufhdgu*Y%YB$*ruihg";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
}

module.exports = router;