const express = require("express");
const bodyParser = require("body-parser");
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cloudinary = require('cloudinary');
const router = express.Router();

const Storage = require("../model/Storage");

cloudinary.config({
    cloud_name: 'ddbvrf8lf',
    api_key: '544275783565819',
    api_secret: 'c6e5peIp3c3cjwLh5U7opTSSAps'
});

router.use(express.static("public"));
router.use(session({
    secret: "webtj947tb$*$)974e",
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());

router.get("/",
    async (req, res) => {
        try {
            let storage = await Storage.find();
            res.render("./../views/goods", {object: storage, user: req.user});
        }catch (err){
            if(err) throw err;
        }
    });

router.get("/add",
    async (req, res) => {
        if(req.user){
            res.render("./../views/add", {user: req.user});
        }else{
            res.redirect("/profile/login");
        }
    });

router.post("/addgoods",
    bodyParser.urlencoded({extended: false}),
    async (req, res) => {
        let name = req.body.goodsName;
        let city = req.body.goodsCity;
        let condition = req.body.goodsCondition;
        let about = req.body.goodsAbout;
        let image;
        let imageID;
        let id = name.replace(/ /g, "_") + "_" + claimId();

        let base64String = 'data:image/png;base64,' + (req.files.goodsImage.data).toString('base64');

        try {
            await cloudinary.v2.uploader.upload(base64String, function(error, result) {
                result ? (image = result.url) : null;
                result ? (imageID = result.public_id) : null;
            });
            let storage = await (new Storage({
                id,
                name,
                city,
                condition,
                about,
                image,
                imageID,
                ownerID: req.user._id
            }));
            await storage.save(function (err) {
                if(err) throw(err);
            });
            res.redirect("/");
        }catch (err){
            console.log(err);
            res.sendStatus(404);
        }
    });

router.post("/delete",
    bodyParser.urlencoded({extended: false}),
    async (req, res) => {
        try {
            let storage = await Storage.findOne({id: req.body.id}, async (err, storage) => {
                if(err) throw err;
                await cloudinary.v2.uploader.destroy(storage.imageID);
                await storage.remove(function (err) {
                    if(err) throw err;
                    res.redirect("/goods");
                });
            });
        }catch (err){
            res.sendStatus(400);
        }
    });

router.get("/:good_id",
    async (req, res) => {
        try {
            let storage = await Storage.findOne({id: req.params.good_id});
            if(req.user){
                res.render("./../views/good", {object: storage, user: req.user, username: req.user.username, role: req.user.role});
            }else{
                res.render("./../views/good", {object: storage, user: req.user, username: "", role: ""});
            }
        }catch (err){
            res.sendStatus(404);
        }
    });

//functions
function claimId(){
    return Math.floor(Math.random() * (1000000000));
}

module.exports = router;