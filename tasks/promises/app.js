function askQuestion() {
    process.stdout.write("Input city name: ");
}

const testFolder = "./";
const https = require("https");
const fs = require("fs-promise");
const fetch = require("fetch");

let found = false;

function processInput(buffer) {
    const inputString = buffer.toString().trim();
    console.log(`Entered city: "${inputString}"`);
    if (inputString) {
        readFile(inputString);
        if (!found) {
            getCityInfo(inputString)
                .then(city => {
                    //console.log(city);
                    jsonCityParse(city, inputString);
                    //askQuestion();
                });
        }
        found = false;
    } else {
        console.log(`Exit.`);
        process.stdin.end();
    }
}

function jsonCityParse(jsonString, cityName) {
    let check = false;
    JSON.parse(jsonString,
        (key, value) => {
            if (key === "href" && check === false) {
                check = true;
                return new Promise((resolve, reject) => {
                    https.get(value,
                        (response) => {
                            if (response.statusCode < 200 || response.statusCode > 299) {
                                reject(new Error(`Failed to load page, status code: ${response.statusCode}`));
                            };

                            const body = [];

                            response.on("data", (chunk) => body.push(chunk));
                            response.on("end", () => {
                                console.log("\nInfo from Web: \n");
                                resolve(fs.writeFile(cityName + ".txt",
                                    JSON.parse(body, null, 4),
                                    (err) => {
                                        if (err) throw err;
                                    }));
                                showNormalInfo(body);
                                askQuestion();
                            });
                        });
                });
            }
        });
}

function showNormalInfo(string) {
    JSON.parse(string, (k, v) => {
        //govnokod 
        if ((k === "full_name" ||
                k === "geoname_id" ||
                k === "geohash" ||
                k === "latitude" ||
                k === "longitude" ||
                k === "name" ||
                k === "population") &&
            k !== "name") {
            const buffer = k + ": " + v;
            string += buffer + " " + "\r\n";
            console.log(buffer);
        }
    });
}

function getCityInfo(cityName) {
    return new Promise((resolve, reject) => {
        https.get(`https://api.teleport.org/api/cities/?search=${cityName}`,
            (response) => {
                if (response.statusCode < 200 || response.statusCode > 299) {
                    reject(new Error(`Failed to load page, status code: ${response.statusCode}`));
                };

                const body = [];

                response.on("data", (chunk) => body.push(chunk));
                response.on("end", () => resolve(body.join("")));
            });
    });
}

function readFile(fileName) {
    fs.readdirSync(testFolder).forEach(file => {
        if (file === fileName + ".txt") {
            found = true;
        }
    });
    const promise = fs.readFile(fileName + ".txt");
    promise.then(fileText => {
            console.log(`\r\nWas readed from ${fileName}\n` + showNormalInfo(fileText));
            askQuestion();
    })
    .catch(err => console.log(`async error: ${String(err)}\n`));
}

process.stdin.addListener("data", processInput);

askQuestion();
