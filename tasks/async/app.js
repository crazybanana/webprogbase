function askQuestion() {
    process.stdout.write("Input city name: ");
}

const testFolder = "./";
const fs = require("fs");
const https = require("https");

let found = false;

function processInput(buffer) {
    const inputString = buffer.toString().trim();
    console.log(`Entered city: "${inputString}"`);
    if (inputString) {
        getCityInfoFromFile(inputString);
        if (!found) {
            https.request("https://api.teleport.org/api/cities/?search=" + inputString, (response) => {
                let str = "";
                response.on("data", (chunk) => {
                    str += chunk;
                });

                let buff = "";
                response.on("end", () => {
                    let check = false;
                    JSON.parse(str, (key, value) => {
                        if (key === "href" && check === false) {
                            check = true;

                            https.get(value, (res) => {
                                res.on("data", (d) => {
                                    buff += d;
                                });
                                
                                res.on("end", () => {
                                    console.log("\r\nInfo from Web:\r\n");
                                    fs.writeFile(inputString + ".txt", buff, (err) => {
                                        if (err) throw err;
                                    });
                                    found = false;
                                    showNormalInfo(buff);
                                    askQuestion();
                                });
                            });
                        }
                    });
                });
            }).end();
        }
        found = false;
    } else {
        // exit
        console.log(`Exit.`);
        process.stdin.end();  // stop listening input
    }
}

function showNormalInfo(string) {
    JSON.parse(string, (k, v) => {
        //govnokod 
        if ((k === "full_name" ||
            k === "geoname_id" ||
            k === "geohash" ||
            k === "latitude" ||
            k === "longitude" || 
            k === "name" ||
            k === "population") &&
            k !== "name") {
            const buffer = k + ": " + v;
            string += buffer + " " + "\r\n";
            console.log(buffer);
        }
    });
}

function getCityInfoFromFile(inputString) {
    fs.readdirSync(testFolder).forEach(file => {
        if (file === inputString + ".txt") {
            found = true;
            fs.readFile(file, "utf8", (err, data) => {
                if (err) throw err;
                console.log("\r\nWas readed from " + file + "\r\n");
                showNormalInfo(data);
                askQuestion();
            });
        }
    });
}

process.stdin.addListener("data", processInput);

askQuestion();
